/**
 * Mortgage Calculator. 
 * @author Jonas Graterol <jonasgraterol@gmail.com>
 *
 * Description: 
 * developed to test skills in JS, CSS, HTML
 * 
 */

// Global var keep validation status
areerrors = false;
	
/**
 * Global variable to check which browser is running Mortgage Calculator
 * and thus be able to execute specific functions in each browser
 */
navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

	
/**
 * Description: 
 * this function synchronizes the value of the inputs range sliders with the numerical input to which corresponds
 *
 * @param {object}   input       DOM input range
 */
function updateInputValue(input) {
  document.getElementById('input'+input.id).value = input.value;
}

/**
 * Description: 
 * this function modifies the style of the range sliders to provide a better UX in webkit and moz browsers
 *
 * @fires   updateInputValue
 * @listens input[range]:onchange
 * 
 * @param {object}   input       DOM input range
 * 
 */
function changeBg(input) {
  //RUN ONLY IF BROWSER IS NOT EDGE OR IE
  if(navigator.sayswho.indexOf('Edge') == -1 && navigator.sayswho.indexOf('IE') == -1) {
    var value = (input.value - input.min)/(input.max - input.min);
    input.style.backgroundImage = [
      '-webkit-gradient(',
        'linear, ',
        'left top, ',
        'right top, ',
        'color-stop(' + value + ', #1091cc), ',
        'color-stop(' + value + ', #d8d8d8)',
      ')'
    ].join('');
    input.style.borderRadius = '100px';
  }
  updateInputValue(input);
}

/**
 * Description: 
 * performs the mathematical calculations of the mortgage
 *
 * @fires   expandResultDiv
 * @listens form:submit
 * 
 */
function calculate() {
	var rangeMortage = parseFloat(document.getElementById('rangeMortage').value);
	var rangeInterest = parseFloat(document.getElementById('rangeInterest').value);
	var loanAmount = parseFloat(document.getElementById('loanAmount').value);
	var annualTax = parseFloat(document.getElementById('annualTax').value);
	var annualInsurance = parseFloat(document.getElementById('annualInsurance').value);

	// The formulas were used exactly as they were delivered in the requirements file for the test

	// FORMULA: ((interestRate / 100) / 12) * loanAmount / (1-Math.pow((1 + ((interestRate / 100)/12)), -yearsOfMortgage*12))
	var principleInterest = parseFloat(((rangeInterest / 100) / 12) * loanAmount / (1-Math.pow((1 + ((rangeInterest / 100)/12)), -rangeMortage*12))).toFixed(2);
	document.getElementById('resultprincipleInsterest').innerHTML = '$ '+principleInterest;
	document.getElementById('resultprincipleInsterest').classList.add('active');

		// FORMULA: annualTax / 12
	var tax = parseFloat(annualTax/12).toFixed(2);
	document.getElementById('resultTax').innerHTML = '$ '+tax;
	document.getElementById('resultTax').classList.add('active');

		// FORMULA: annualInsurance / 12
	var insurance = parseFloat(annualInsurance/12).toFixed(2);
	document.getElementById('resultInsurance').innerHTML = '$ '+insurance;
	document.getElementById('resultInsurance').classList.add('active');

		// FORMULA: principleAndInterests + Tax + Insurance
	var total = (parseFloat(principleInterest) + parseFloat(tax) + parseFloat(insurance)).toFixed(2);
	document.getElementById('resultTotal').innerHTML = '$ '+total;
	document.getElementById('resultTotal').classList.add('active');

	document.getElementById('btnCalculate').value = 'RECALCULATE';
	expandResultDiv();
}

/**
 * Description: 
 * resets the results panel values
 */
function clearResults() {
  
	document.getElementById('resultprincipleInsterest').innerHTML = '$ - -';
	document.getElementById('resultprincipleInsterest').classList.remove('active');
	document.getElementById('resultTax').innerHTML = '$ - -';
	document.getElementById('resultTax').classList.remove('active');
	document.getElementById('resultInsurance').innerHTML = '$ - -';
	document.getElementById('resultInsurance').classList.remove('active');
	document.getElementById('resultTotal').innerHTML = '$ - -';
	document.getElementById('resultTotal').classList.remove('active');

	document.getElementById('btnCalculate').value = 'CALCULATE';
  
}

/**
 * Description: 
 * add animations and effects to show the results of the calculation
 * runs only in responsive mode (breakpoint < 600px)
 */
function expandResultDiv() {
	if(document.documentElement.clientWidth < 600) {
		
		setTimeout(function() {
			//RUN ONLY IF BROWSER IS NOT EDGE OR IE
  			if(navigator.sayswho.indexOf('Edge') == -1 && navigator.sayswho.indexOf('IE') == -1) {
				window.scroll({ top: 15000, left: 0, behavior: 'smooth' });
			}
			else {
				document.body.scrollTop = document.body.scrollHeight;
			}
			
		}, 500);
			
		setTimeout(function() {
			document.getElementById('divResult').style.height = '400px';
			document.getElementById('divResult').style.width = '100%';
			document.getElementById('divResult').style.margin = '0px';
			document.getElementById('divResult').style.backgroundColor = '#eaf6fa';
		}, 1500);

		setTimeout(function() {
			document.getElementById('divResult').style.height = 'fit-content';
		}, 3500);
		
	}
}

/**
 * Description: 
 * Validate the form prior to performing the calculation
 * Handles error states and UX information
 */
function validateForm()
{
	areerrors = false;
	var inputs = document.querySelectorAll("input.validate");
	for(var i=0; i<inputs.length; i++) {
		//CLEAR ERRORS
		inputs[i].classList.remove("error");
		document.getElementById('error'+inputs[i].id).style.display = 'none';
		
		//VALIDATIONS
		if(inputs[i].value.trim() == '' || isNaN(inputs[i].value)) {
		areerrors = true;
		inputs[i].classList.add("error");
		document.getElementById('error'+inputs[i].id).style.display = 'inline';
		}
	}

	if(!areerrors) {
		calculate();
	}
	else {
		clearResults();
	}
}

/**
 * Description: 
 * Controls the form validation call
 *
 * @fires   validateForm
 * @listens input[submit]:click
 * 
 */
document.getElementById("btnCalculate").addEventListener(
	"click", 
	//(e) => { 
	function(e) {
		e.preventDefault();
		validateForm();
	}
);

/**
 * Description: 
 * Updates the visual information of the range sliders when loading the page 
 */
onload = function() {
	changeBg(document.getElementById('rangeMortage'));
	changeBg(document.getElementById('rangeInterest'));
}